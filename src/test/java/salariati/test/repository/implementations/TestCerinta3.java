package salariati.test.repository.implementations;

import org.junit.Before;
import org.junit.Test;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;

import static org.junit.Assert.*;
import java.util.List;

public class TestCerinta3 {
    EmployeeRepositoryImpl repo;

    @Before
    public void setUp() throws Exception {
        repo = new EmployeeRepositoryImpl();

    }

    @Test
    public void test1() throws EmployeeException {
        List<Employee> list = repo.getEmployeeList();
        int salari1 = Integer.parseInt(list.get(0).getSalary());
        int salari2 = Integer.parseInt(list.get(1).getSalary());
        assertEquals(salari1 >= salari2, true);
    }


    @Test
    public void test2() throws EmployeeException {
        List<Employee> list = repo.getEmployeeList();
        int salari1 = Integer.parseInt(list.get(0).getSalary());
        int salari2 = Integer.parseInt(list.get(1).getSalary());
        assertEquals(salari1 < salari2, false);
    }

    /*
poop;denisa;2890101415827;TEACHER;4000
pooooooooooooooooooooop;ana;2890101416562;CONFERENTIAR;4000
nume;ioan;1900527061443;LECTURER;3000
nume;ioooooooooooooooooooooa;2931121044449;TEACHER;3000
nume;iooooooooooooooooooooooa;2911107341788;LECTURER;3000
pop;ana;2940516376309;LECTURER;2000
pop;ana;1900120148661;LECTURER;2147483646
caraian;ioana;2960212125795;ASISTENT;2000
pop;maria;2961603111222;LECTURER;3000

     */
}