package salariati.test.validator;

import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;

import java.util.List;

import static org.junit.Assert.*;

public class TestTopDown {
    EmployeeRepositoryImpl repo;

    @Before
    public void setUp() throws Exception {
        repo = new EmployeeRepositoryImpl();

    }

    @Test(expected = EmployeeException.class)
    public void testA() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("a222;ioana;2961603111223;ASISTENT;2000",0);
        repo.addEmployee(e);
    }

    @Test
    public void testB() throws Exception {
        Employee e = new Employee();
        e = e.getEmployeeFromString("rotar;alex;1900527061222;LECTURER;3000", 0);
        repo.addEmployee(e);
        Employee newEmployee = new Employee();
        newEmployee = newEmployee.getEmployeeFromString("rotar;alex;1900527061222;CONFERENTIAR;3000", 0);
        repo.modifyEmployee(e,newEmployee);
        Employee empl = repo.findEmployeeByCnp("1900527061222");
        assertEquals(empl.getFunction(), DidacticFunction.CONFERENTIAR);
    }

    @Test
    public void testC() throws Exception {
        Employee e = new Employee();
        e = e.getEmployeeFromString("marinescu;ioan;1900527061222;LECTURER;3000",0);
        repo.addEmployee(e);
        Employee newEmployee = new Employee();
        newEmployee = newEmployee.getEmployeeFromString("marinescu;ioan;1900527061222;CONFERENTIAR;3000",0);
        repo.modifyEmployee(e,newEmployee);
        List<Employee> listEmployee = repo.getEmployeeByCriteria();
        assertEquals(listEmployee.size() != 0, true);
        for(Employee empl : listEmployee)
            if(empl.getCnp().equals("1900527061222"))
                assertEquals(empl.getFunction(), DidacticFunction.CONFERENTIAR);
    }
}
