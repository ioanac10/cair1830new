package salariati.restController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.model.RestEmployee;
import salariati.repository.implementations.EmployeeRepositoryImpl;


@Controller
public class RestController {

    private final static String HOME = "home";

    private EmployeeRepositoryImpl employeeRepository = new EmployeeRepositoryImpl ();
    private EmployeeController employeeController = new EmployeeController(employeeRepository);

    @GetMapping(value = "/")
    public String getHomePage() {
        return HOME;
    }

    @PostMapping(value = "/add")
    public ResponseEntity<Void> add(@RequestBody RestEmployee employee) {
        boolean check = false;
        Employee employeeApp = new Employee();
        employeeApp.setFistName(employee.getFirstName());
        employeeApp.setLastName(employee.getLastName());
        employeeApp.setCnp(employee.getCnp());
        employeeApp.setSalary(employee.getSalary());

        if (employee.getFunction().equals("asistent")) {
            employeeApp.setFunction(DidacticFunction.ASISTENT);
            check = true;
        }
        if (employee.getFunction().equals("lecturer")) {
            employeeApp.setFunction(DidacticFunction.LECTURER);
            check = true;
        }
        if (employee.getFunction().equals("teacher")) {
            employeeApp.setFunction(DidacticFunction.TEACHER);
            check = true;
        }
        if (employee.getFunction().equals("conferentiar")) {
            employeeApp.setFunction(DidacticFunction.CONFERENTIAR);
            check = true;
        }
        if (check == false) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }
        try {
            employeeController.addEmployee(employeeApp);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
