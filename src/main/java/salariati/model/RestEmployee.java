package salariati.model;

public class RestEmployee {
    /**
     * The first name of the employee
     */
    private String firstName;

    /**
     * The last name of the employee
     */
    private String lastName;

    /**
     * The unique id of the employee
     */
    private String cnp;

    /**
     * The didactic function of the employee inside the university
     */
    private String function;

    /**
     * The salary of the employee
     */
    private String salary;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }
}
