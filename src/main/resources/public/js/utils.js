/**
 *
 * @param $form - form data
 * @returns json with form elements
 */
function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
};

/**
 * Display alert box.
 * @param text that will be put in paragraph
 */
function showModalDialog(text) {
    $('#myModal').modal('show');
    $('#modalText').text(text);
}

/**
 * Check if a specific string is blank.
 * @param str
 * @returns {boolean}
 */
function isEmpty(str) {
    return (!str || 0 === str.length);
}