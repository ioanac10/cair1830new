$(document).ready(function () {

    $("#addNewEmployeeForm").submit(function (e) {

        var url = 'http://localhost:8080/add';

        var $form = $("#addNewEmployeeForm");
        var data = getFormData($form);

        if (isEmpty(data.firstName) || isEmpty(data.lastName) || isEmpty(data.cnp) || isEmpty(data.function) || isEmpty(data.salary)) {
            showModalDialog("Please complete all fields!");
            e.preventDefault();
        } else {
            $.ajax({
                type: "POST",
                url: url,
                contentType: 'application/json',
                data: JSON.stringify(data),
                success: function (data, textStatus, xhr) {
                    if (xhr.status == 200) {
                        showModalDialog("SUCCESS!");
                    }
                },
                error: function (xhr) {
                    if (xhr.status == 404) {
                        showModalDialog("ERROR!");
                    }
                }
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        }
    });
});